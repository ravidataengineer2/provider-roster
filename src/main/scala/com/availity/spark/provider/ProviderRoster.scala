package com.availity.spark.provider

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructType, DateType, StringType}

import org.apache.spark.sql.functions.{count, lit, array, collect_list, col, month, avg}

object ProviderRoster  {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .appName("Provider Visits")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._

    // Load the CSV files
    val providersDF = spark.read.option("header", "true").csv("data/providers.csv")
    val visitsDF = spark.read.option("header", "true").csv("data/visits.csv")

    // Problem 1: Total number of visit per provider
    val visitsPerProviderDF = visitsDF
      .groupBy("provider_id")
      .agg(count("visit_id").as("total_visits"))

    val result1DF = visitsPerProviderDF
      .join(providersDF, "provider_id")
      .select("provider_id", "provider_name", "specialty", "total_visits")

    // Output the result partitioned by specialty
    result1DF.write
      .partitionBy("specialty")
      .json("output/total_visits_per_provider")

    // Problem 2: Total number of visits per provider per month
    val visitsPerMonthDF = visitsDF
      .withColumn("month", date_format(col("date_of_service"), "yyyy-MM"))
      .groupBy("provider_id", "month")
      .agg(count("visit_id").as("total_visits"))

    // Output the result
    visitsPerMonthDF.write
      .json("output/total_visits_per_provider_per_month")

    spark.stop()
  }
}

